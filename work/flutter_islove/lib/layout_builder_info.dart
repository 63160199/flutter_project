import 'package:flutter/material.dart';

class LayoutBuilderInfo extends StatelessWidget {
  const LayoutBuilderInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final maxHeight = constraints.maxHeight;
        return Center(
          child: Text(
            'The max height is $maxHeight pixels',
            style: const TextStyle(fontSize: 24),
          ),
        );
      },
    );
  }
}
